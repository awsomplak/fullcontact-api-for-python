# FULLCONTACT PYTHON API

[![N|Solid](https://cdn.shopify.com/s/files/1/0026/2379/9359/products/AW-LOGO-TEE-WHITE-2_55x.jpg)](https://www.facebook.com/profile.php?id=100000659310340)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/awsomplak/fullcontact-api-for-python)

A Python interface for the FullContact API
---
### Installation
Cloning the dependencies.

```
$ git clone https://gitlab.com/awsomplak/fullcontact-api-for-python.git fullcontact
```
---
### Optional Search:
| Params | Properties |
| ------------ | ------------ |
| Email | The email that would be searched  |
| Phone | The phone number that would be searched. </br> Use country code. ex: +62 for Indonesian phone number |
| Domain | The company information from domain, |
| Twitter | The username or url of twitter account |

---
### Params:
| Params | Value |
| ------------ | ------------ |
| email | example@mail.com |
| phone | +621234567890 |
| domain | domain@domain.tld |
| twitter | twitter@name or twitter@https://twitter.com/name |

---
### Example Usage:
```
. . .
from fullcontact import do_search
. . .

def fullcontact(_type, _input):
    return do_search(_type, _input)

_input = request.form['input']
_type = request.form['type']
if("twitter@" in _input):
	requested = _input.replace('twitter@', '@')
	result = jsonify(fullcontact(_type, requested))
	return result

if("domain@" in _input):
	requested = _input.replace('domain@', '')
	result = jsonify(fullcontact(_type, requested))
	return result
	
else:
result = jsonify(fulcontact(_type, requested))
return result
```
---
### License
###### MIT &copy;2019 ajiwahyusomplak@gmail.com
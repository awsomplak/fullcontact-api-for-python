import requests
import json
import config

# Fullcontact API (AWSomplak)
# Optional Search:
# Email, Phone, Domain, Twitter
# Params -> email, phone, domain, twitter
# Value -> example@mail.com, +62123456890, domain@example.com, twitter@name

def do_search(type_search, search):
	# Data to be sent to API
	data = {type_search : search}

	# Header Request
	headers = {"content-type": "application/json",
	           "Authorization": config.API_KEY}

	# Request Enrich Type
	if type_search == "domain":
		enrich_type = "company"
		enrich = config.API_ENDPOINT+enrich_type+".enrich"

	else:
		enrich_type = "person"
		enrich = config.API_ENDPOINT+enrich_type+".enrich"

	# Sending POST Request & Saving Response as Response Object
	r = requests.post(url=enrich, data=json.dumps(data), headers=headers)

	# Extracting Response Text
	result = r.text
	hasil = json.loads(result);

	looping = True
	while looping:
		status = hasil.get('status')

		# Status is Being Processed
		if status == 202:
			looping = True
			return do_search(type_search, search)

		# Status Notfound
		if status == 404:
			looping = False
			data = {'status': 404, 'messages': 'Profile Not Found', 'data':[]}
			return data

		# Status Error
		if status == 400:
			looping = False
			data = {'status': 400, 'messages': hasil.get('message'), 'data':[]}
			return data

		# The request was successful
		else:
			looping = False
			data = {'status': 200, 'type': 'flc', enrich_type: hasil}
			return data
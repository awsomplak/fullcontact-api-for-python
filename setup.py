"""
fullContact.py
--------------
Simple Python interface for FullContact API.
"""
from setuptools import setup


setup(
    name='FullContact API for Python',
    version='0.0.1',
    dependency_links=['https://gitlab.com/awsomplak/fullcontact-api-for-python'],
    license='MIT',
    author=['AWSomplak'],
    author_email=['ajiwahyusomplak@gmail.com'],
    description='A Python interface for the FullContact API',
    long_description='A Python interface for the FullContact API',
    packages=['fullcontactapi'],
    zip_safe=False,
    platforms='any',
    install_requires=[
        'requests',
        'json'
    ],
    include_package_data=True,
    tests_require=[
        'nose>=1.0',
        'flake8<=2.6.2'  # Last version of flake8 to support Python 2.6
    ],
    test_suite='nose.collector',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)